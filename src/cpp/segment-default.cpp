#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/mesh_segmentation.h>
#include <CGAL/property_map.h>
#include <iostream>
#include <fstream>

#include <string>
#include <sstream>
#include <vector>

typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef CGAL::Polyhedron_3<Kernel> Polyhedron;

int main(int argc, char* argv[])
{
    // We need one input which is the file name of the mesh we want to process
    if ( argc != 2 ) {
	return EXIT_FAILURE;
    }

    // Initialize Polyhedron
    Polyhedron mesh;
    std::ifstream input(argv[1]);
    if ( !input ) {
	std::cerr << argv[1] << " is not a valid file name." << std::endl;
	return EXIT_FAILURE;
    }
    else if ( !(input >> mesh) ) {
	std::cerr << "Cannot read input." << std::endl;
	return EXIT_FAILURE;
    }
    else if ( mesh.empty() ) {
	std::cerr << "Mesh is empty." << std::endl;
	return EXIT_FAILURE;
    }
    std::cout << "Successfully load mesh from " << argv[1]  << std::endl;

    // Create a property-map for segment-ids
    typedef std::map<Polyhedron::Facet_const_handle, std::size_t> Facet_int_map;
    Facet_int_map internal_segment_map;
    boost::associative_property_map<Facet_int_map> segment_property_map(internal_segment_map);

    // Calculate SDF values and segment the mesh using default parameters.
    std::size_t number_of_segments = CGAL::segmentation_via_sdf_values(mesh, segment_property_map);
    std::cout << "Number of segments: " << number_of_segments << std::endl;

    // Get the basename of the filename
    std::string filename = argv[1];
    const std::size_t peroid_idx = filename.rfind('.');
    if ( std::string::npos != peroid_idx ) {
	filename.erase(peroid_idx);
    }
    filename += ".segmentation.data";

    std::ofstream out(filename.c_str());
    std::string delim = "";
    for (Polyhedron::Facet_const_iterator facet_it = mesh.facets_begin();
	 facet_it != mesh.facets_end(); ++facet_it) {
	out << delim << segment_property_map[facet_it];
	delim = " ";
    }
    return EXIT_SUCCESS;
}
