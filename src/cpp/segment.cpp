#include <boost/program_options.hpp>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/mesh_segmentation.h>

#include <CGAL/property_map.h>

#include <iostream>
#include <fstream>

typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef CGAL::Polyhedron_3<Kernel> Polyhedron;

namespace po = boost::program_options;

int main(int argc, char* argv[])
{
    // Parse command line arguments
    std::string filename;
    int nrays;
    double cone_angle;
    int nclusters;
    double lambda;
    
    po::options_description desc("Options");
    desc.add_options()
	("help", "Show this help message and exit")
	("file", po::value<std::string>(&filename), "File name (in off format)")
	("nrays", po::value<int>(&nrays)->default_value(25), "Number of rays casted per facet")
	("cone-angle", po::value<double>(&cone_angle)->default_value(2.0*CGAL_PI/3.0), "Cone opening angle")
	("nclusters", po::value<int>(&nclusters)->default_value(4), "Number of clusters used in soft clustering")
	("lambda", po::value<double>(&lambda)->default_value(0.3), "Importance of surface features, suggested to be in [0, 1]");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
    if( vm.count("help") ) {
	std::cout << desc << std::endl;
	return EXIT_SUCCESS;
    }
    if( !vm.count("file") ) {
	std::cerr << "Please specify the input file name (in off format)" << std::endl;
	std::cout << desc << std::endl;
	return EXIT_FAILURE;
    }

    // Initialize Polyhedron
    Polyhedron mesh;
    std::ifstream input(filename.c_str());
    if( !input ) {
	std::cerr << filename << " is not a valid file name." << std::endl;
	return EXIT_FAILURE;
    }
    else if ( !(input >> mesh) ) {
	std::cerr << "Cannot read input." << std::endl;
	return EXIT_FAILURE;
    }
    else if ( mesh.empty() ) {
	std::cerr << "Mesh is empty." << std::endl;
	return EXIT_FAILURE;
    }
    std::cout << "Successfully load mesh from " << filename << std::endl;

    // Create a property-map for SDF values
    typedef std::map<Polyhedron::Facet_const_handle, double> Facet_double_map;
    Facet_double_map internal_sdf_map;
    boost::associative_property_map<Facet_double_map> sdf_property_map(internal_sdf_map);

    // It is possible to compute the raw SDF values and post-process them using the
    // following lines:
    CGAL::sdf_values(mesh, sdf_property_map, cone_angle, nrays, false);
    
    // Compute SDF values using default parameters for number of rays, and cone angle
    CGAL::sdf_values(mesh, sdf_property_map);

    // Create a property-map for segment-ids
    typedef std::map<Polyhedron::Facet_const_handle, std::size_t> Facet_int_map;
    Facet_int_map internal_segment_map;
    boost::associative_property_map<Facet_int_map> segment_property_map(internal_segment_map);
    
    std::size_t number_of_segments = CGAL::segmentation_from_sdf_values(mesh, sdf_property_map, segment_property_map, nclusters, lambda);
    std::cout << "Number of segments: " << number_of_segments << std::endl;

    // Get the basename of the filename
    const std::size_t peroid_idx = filename.rfind('.');
    if ( std::string::npos != peroid_idx ) {
	filename.erase(peroid_idx);
    }
    filename += ".segmentation.data";

    std::ofstream out(filename.c_str());
    std::string delim = "";
    for (Polyhedron::Facet_const_iterator facet_it = mesh.facets_begin();
	 facet_it != mesh.facets_end(); ++facet_it) {
	out << delim << segment_property_map[facet_it];
	delim = " ";
    }
    return EXIT_SUCCESS;
}
