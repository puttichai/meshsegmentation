import openravepy as orpy
import trimesh as tm
import segmentation
import numpy as np
from itertools import chain # for unpacking nested iterables

env = orpy.Environment()
env.SetViewer('qtcoin')

filename = '../../models/woodbench.off'
bodies, meshes = segmentation.visualize(env, filename, returnmeshes=True)

epsilon = 1e-8
# Test boxifying
bbs = [mesh.bounding_box_oriented for mesh in meshes]

def norm(vector):
    return np.sqrt(np.dot(vector, vector))

def normalize(vector):
    length = np.sqrt(np.dot(vector, vector))
    if length > 0:
        return vector/length
    assert(False)
        
def areparallel(vect1, vect2):
    v1 = normalize(vect1)
    v2 = normalize(vect2)
    if abs(abs(np.dot(v1, v2)) - 1) < epsilon:
        return True
    else:
        return False

def bbtoorbox(bb, orenv, kinbodyname='b', color=[0.5, 1, 0.5]):
    """converting a trimesh's box primitive to an openrave box.

    (px, py, pz): the object's origin in the world frame
    (dx, dy, dz): half-extents of the box in x, y, z, respectively    
    """
    p = np.mean(bb.vertices, axis=0) # the bb's center
    axes = [] # contains three axes of the local frame
    dim = [] # half-extents of the box along axes speficied in axes
    
    # Although trimesh already provides face_normals, it is easier for us to calculate the
    # normals this way since we also need the distance from each facet to the center
    for facet in bb.facets():
        faces = [bb.faces[iface] for iface in facet] # faces included in this facet
        vertex_indices = sorted(set(chain.from_iterable(faces)))
        vcenter = np.mean([bb.vertices[i] for i in vertex_indices], axis=0)
        direction = (vcenter - p)
        axis = normalize(direction)
        parallel = False
        for existingaxis in axes:
            if areparallel(axis, existingaxis):
                parallel = True
                break
        if not parallel:
            axes.append(axis)
            dim.append(norm(direction))
            if len(axes) == 3:
                break
    assert (len(axes) == 3), "Unable to compute box's local frame"
    # Make sure the axes respect the right-hand rule
    if np.dot(np.cross(axes[0], axes[1]), axes[2]) < 0:
        axes.append(axes.pop(1)) # swap the last two elements
        dim.append(dim.pop(1)) # swap the last two elements

    Tb = np.vstack([np.vstack([axes[0], axes[1], axes[2], p]).T, np.array([0, 0, 0, 1])])
        
    orbox = orpy.RaveCreateKinBody(orenv, '')
    orbox.InitFromBoxes(np.array([[0.0, 0.0, 0.0, dim[0], dim[1], dim[2]]]))
    orbox.SetName(kinbodyname)
    orenv.Add(orbox, True) # allow name clash (fixing it by appending numbers to the name)
    orbox.SetTransform(Tb)
    return orbox, Tb
