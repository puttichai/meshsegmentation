import openravepy as orpy
import segmentation
import argparse
import IPython
epsilon = 1e-8


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Visualize segmented mesh models')
    parser.add_argument('-f', '--filename', type=str,
                        help="File name of the object to be visualized (in off format)")
    parser.add_argument('-vw', '--viewername', type=str, default='qtcoin',
                        help="Name of OpenRAVE viewer; select from ['qtcoin', 'qtosg']")
    args = parser.parse_args()
    filename = args.filename
    vwname = args.viewername
    
    env = orpy.Environment()
    env.SetViewer(vwname)
    bodies, meshes = segmentation.visualize(env, filename, returnmeshes=True)
    bbs = [mesh.bounding_box_oriented for mesh in meshes] # bounding boxes computed by trimesh
    IPython.embed(header="visualize.py")
