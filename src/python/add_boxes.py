# snippets for adding boxes to models

import numpy as np
import trimesh
import openravepy as orpy
import segmentation
from pymanip.planningutils import utils
env = orpy.Environment()
env.SetViewer('qtcoin')

def box(dx, dy, dz, transparency=0.4, scale=1.0):
    orbox = orpy.RaveCreateKinBody(env, '')
    orbox.InitFromBoxes(np.array([[0.0, 0.0, 0.0, dx*scale, dy*scale, dz*scale]]))
    orbox.SetName('b')
    env.Add(orbox, True) # allow name clash (fixing it by appending numbers to the name)
    for l in orbox.GetLinks():
        for g in l.GetGeometries():
            g.SetTransparency(transparency)
    return orbox

def CreateXML(b, T, isEnabled=False, isVisible=False):
    x, y, z = b.GetLinks()[0].GetGeometries()[0].GetBoxExtents()
    pose = orpy.poseFromMatrix(T)
    enable = "true" if isEnabled else "false"
    visible = "true" if isVisible else "false"
    data = """<body name="{0}" enable="{1}">
  <geom type="box" render="{2}">""".format(b.GetName(), enable, visible)

    data += """
    <extents>{0} {1} {2}</extents>
  </geom>
  <quat>{3} {4} {5} {6}</quat>
  <translation>{7} {8} {9}</translation>
</body>\n""".format(x, y, z, *pose)
    return data

import IPython; IPython.embed(header="experimental area")


##########################################################################################
#
# Ikea Stefan chair
#
filename = '../../models/ikea-stefan.off'
bodies, meshes = segmentation.visualize(env, filename, returnmeshes=True)
bbs = [mesh.bounding_box_oriented for mesh in meshes]
bodieslist = [36, 37, 38, 44, 45, 46, 47, 50, 51, 52, 54] # list of bodies which are ok to compute bb
orboxes = [segmentation.bbtoorbox(bbs[i], env) for i in bodieslist]

# boxes for the seat
b = box(0.17, 0.05, 0.005)
b.SetTransform(np.array([[  9.94588453e-01,   1.03893254e-01,   1.23850361e-08,   5.29624186e-02],
                         [ -1.03893254e-01,   9.94588453e-01,  -6.45097570e-10,  -1.57253280e-01],
                         [ -1.23850352e-08,  -6.45115111e-10,   1.00000000e+00,   4.38219637e-01],
                         [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]]))
orboxes.append((b, b.GetTransform()))

b = box(0.17, 0.05, 0.005)
b.SetTransform(np.array([[  9.94588453e-01,  -1.03893254e-01,   1.23850361e-08,   5.29624186e-02],
                         [  1.03893254e-01,   9.94588453e-01,  -6.45097570e-10,   1.57253280e-01],
                         [ -1.23850352e-08,  -6.45115111e-10,   1.00000000e+00,   4.38219637e-01],
                         [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]]))
orboxes.append((b, b.GetTransform()))

b = box(0.05, 0.20, 0.005)
b.SetTransform(np.array([[  1.00000000e+00,  -0.00000000e+00,  -2.19622226e-14,   2.03011841e-01],
                         [  0.00000000e+00,   1.00000000e+00,  -0.00000000e+00,   0.00000000e+00],
                         [  2.19622226e-14,   0.00000000e+00,   1.00000000e+00,   4.38302279e-01],
                         [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]]))
orboxes.append((b, b.GetTransform()))

b = box(0.05, 0.14, 0.005)
b.SetTransform(np.array([[  1.00000000e+00,  -0.00000000e+00,  -2.19622226e-14,  -1.25400000e-01],
                         [  0.00000000e+00,   1.00000000e+00,  -0.00000000e+00,   0.00000000e+00],
                         [  2.19622226e-14,   0.00000000e+00,   1.00000000e+00,   4.38302279e-01],
                         [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]]))
orboxes.append((b, b.GetTransform()))

# back (upper part)
b = box(0.07, 0.0125, 0.03)
b.SetTransform(np.array([[ 0.1270614 , -0.97619269,  0.17579317, -0.22812344],
                         [-0.99126675, -0.13127695, -0.01251394, -0.07285906],
                         [ 0.03529361, -0.17266789, -0.98434758,  0.87763393],
                         [ 0.        ,  0.        ,  0.        ,  1.        ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.07, 0.0125, 0.03)
b.SetTransform(np.array([[ -1.27901568e-01,  -9.74076364e-01,   1.86591603e-01,  -2.27579430e-01],
                         [ -9.91489328e-01,   1.30187986e-01,   8.80437057e-10,   7.27037042e-02],
                         [ -2.42919858e-02,  -1.85003583e-01,  -9.82437567e-01,   8.77633929e-01],
                         [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]]))
orboxes.append((b, b.GetTransform()))

# back (lower part)
b = box(0.07, 0.0125, 0.02)
b.SetTransform(np.array([[ 0.1270614 , -0.97619269,  0.17579317, -0.18473849],
                         [-0.99126675, -0.13127695, -0.01251394, -0.07147574],
                         [ 0.03529361, -0.17266789, -0.98434758,  5.81399858e-01],
                         [ 0.        ,  0.        ,  0.        ,  1.        ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.07, 0.0125, 0.02)
b.SetTransform(np.array([[ -1.27901568e-01,  -9.74076364e-01,   1.86591603e-01,  -1.84030831e-01],
                         [ -9.91489328e-01,   1.30187986e-01,   8.80437057e-10,   7.14963898e-02],
                         [ -2.42919858e-02,  -1.85003583e-01,  -9.82437567e-01,   5.81399858e-01],
                         [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]]))
orboxes.append((b, b.GetTransform()))

# frame
b = box(0.017, 0.015, 0.22)
b.SetTransform(np.array([[  9.88065014e-01,  -8.47724955e-16,  -1.54037424e-01,  -1.85130075e-01],
                         [  2.02287065e-15,   1.00000000e+00,   7.47222809e-15,  -1.61016241e-01],
                         [  1.54037424e-01,  -7.69464494e-15,   9.88065014e-01,   7.04181552e-01],
                         [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]]))
orboxes.append((b, b.GetTransform()))

b = box(0.017, 0.015, 0.17)
b.SetTransform(np.array([[  9.94993045e-01,  -5.51865123e-16,   9.99441844e-02,  -1.69144526e-01],
                         [  4.95616911e-16,   1.00000000e+00,   5.87625422e-16,  -1.61016732e-01],
                         [ -9.99441844e-02,  -5.35149180e-16,   9.94993045e-01,   1.71182454e-01],
                         [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]]))
orboxes.append((b, b.GetTransform()))

b = box(0.017, 0.015, 0.075)
b.SetTransform(np.array([[  9.99999762e-01,   6.21698046e-18,  -6.90533215e-04,  -1.52958870e-01],
                         [ -7.85249239e-18,   1.00000000e+00,  -2.36847413e-15,  -1.61018401e-01],
                         [  6.90533215e-04,   2.36847899e-15,   9.99999762e-01,   4.15452212e-01],
                         [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]]))
orboxes.append((b, b.GetTransform()))

b = box(0.017, 0.015, 0.22)
b.SetTransform(np.array([[  9.88065014e-01,  -8.47724955e-16,  -1.54037424e-01,  -1.85130075e-01],
                         [  2.02287065e-15,   1.00000000e+00,   7.47222809e-15,   1.61016241e-01],
                         [  1.54037424e-01,  -7.69464494e-15,   9.88065014e-01,   7.04181552e-01],
                         [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]]))
orboxes.append((b, b.GetTransform()))

b = box(0.017, 0.015, 0.17)
b.SetTransform(np.array([[  9.94993045e-01,  -5.51865123e-16,   9.99441844e-02,  -1.69144526e-01],
                         [  4.95616911e-16,   1.00000000e+00,   5.87625422e-16,   1.61016732e-01],
                         [ -9.99441844e-02,  -5.35149180e-16,   9.94993045e-01,   1.71182454e-01],
                         [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]]))
orboxes.append((b, b.GetTransform()))

b = box(0.017, 0.015, 0.075)
b.SetTransform(np.array([[  9.99999762e-01,   6.21698046e-18,  -6.90533215e-04,  -1.52958870e-01],
                         [ -7.85249239e-18,   1.00000000e+00,  -2.36847413e-15,   1.61018401e-01],
                         [  6.90533215e-04,   2.36847899e-15,   9.99999762e-01,   4.15452212e-01],
                         [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]]))
orboxes.append((b, b.GetTransform()))

for b in bodies:
    env.Remove(b)

for b in env.GetBodies():
    b.Enable(False)
    b.SetVisible(False)
    
chair = env.ReadKinBodyXMLFile(filename)
env.Add(chair)
for l in chair.GetLinks():
    for g in l.GetGeometries():
        g.SetTransparency(0.)


xmldata = """<KinBody name="ikea-stafan">
  <body name="main" type="dynamic">
    <Geom type="trimesh" modifiable="false">
      <data>ikea-stefan.off 1.0</data>
      <render>ikea-stefan.off 1.0</render>
    </Geom>
  </body>\n"""

for b, T in orboxes:
    xmldata += CreateXML(b, T)
xmldata += "</KinBody>"

##########################################################################################
#
# taburet1.off
#
filename = '../../models/taburet1.off'
bodies, meshes = segmentation.visualize(env, filename, returnmeshes=True)
orboxes = []

# box for the seat
b = box(0.2, 0.2, 0.0275)
b.SetTransform(np.array([[ 1.    ,  0.    ,  0.    ,  0.    ],
                         [ 0.    ,  1.    ,  0.    ,  0.    ],
                         [ 0.    ,  0.    ,  1.    ,  0.8075],
                         [ 0.    ,  0.    ,  0.    ,  1.    ]]))
orboxes.append((b, b.GetTransform()))

# boxes for the leg
b = box(0.02, 0.01, 0.39)
b.SetTransform(np.array([[ 1.  ,  0.  ,  0.  , -0.18],
                         [ 0.  ,  1.  ,  0.  , -0.19],
                         [ 0.  ,  0.  ,  1.  ,  0.39],
                         [ 0.  ,  0.  ,  0.  ,  1.  ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.02, 0.01, 0.39)
b.SetTransform(np.array([[ 1.  ,  0.  ,  0.  , -0.18],
                         [ 0.  ,  1.  ,  0.  ,  0.19],
                         [ 0.  ,  0.  ,  1.  ,  0.39],
                         [ 0.  ,  0.  ,  0.  ,  1.  ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.02, 0.01, 0.205)
b.SetTransform(np.array([[ 1.  ,  0.  ,  0.  ,  0.18 ],
                         [ 0.  ,  1.  ,  0.  , -0.19 ],
                         [ 0.  ,  0.  ,  1.  ,  0.205],
                         [ 0.  ,  0.  ,  0.  ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.02, 0.01, 0.205)
b.SetTransform(np.array([[ 1.  ,  0.  ,  0.  ,  0.18 ],
                         [ 0.  ,  1.  ,  0.  ,  0.19 ],
                         [ 0.  ,  0.  ,  1.  ,  0.205],
                         [ 0.  ,  0.  ,  0.  ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.01, 0.01, 0.185)
b.SetTransform(np.array([[ 1.  ,  0.  ,  0.  ,  0.18 ],
                         [ 0.  ,  1.  ,  0.  ,  0.   ],
                         [ 0.  ,  0.  ,  1.  ,  0.595],
                         [ 0.  ,  0.  ,  0.  ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.16, 0.01, 0.02)
b.SetTransform(np.array([[ 1.  ,  0.  ,  0.  ,  0.   ],
                         [ 0.  ,  1.  ,  0.  , -0.19 ],
                         [ 0.  ,  0.  ,  1.  ,  0.02 ],
                         [ 0.  ,  0.  ,  0.  ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.16, 0.01, 0.02)
b.SetTransform(np.array([[ 1.  ,  0.  ,  0.  ,  0.   ],
                         [ 0.  ,  1.  ,  0.  ,  0.19 ],
                         [ 0.  ,  0.  ,  1.  ,  0.02 ],
                         [ 0.  ,  0.  ,  0.  ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.02, 0.18, 0.01)
b.SetTransform(np.array([[ 1.  ,  0.  ,  0.  ,  0.18 ],
                         [ 0.  ,  1.  ,  0.  ,  0.   ],
                         [ 0.  ,  0.  ,  1.  ,  0.40 ],
                         [ 0.  ,  0.  ,  0.  ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

for b in bodies:
    env.Remove(b)

for b in env.GetBodies():
    b.Enable(False)
    b.SetVisible(False)
    
taburet1 = env.ReadKinBodyXMLFile(filename)
env.Add(taburet1)
for l in taburet1.GetLinks():
    for g in l.GetGeometries():
        g.SetTransparency(0.)

xmldata = """<KinBody name="taburet1">
  <body name="main" type="dynamic">
    <Geom type="trimesh" modifiable="false">
      <data>taburet1.off 1.0</data>
      <render>taburet1.off 1.0</render>
    </Geom>
  </body>\n"""

for b, T in orboxes:
    xmldata += CreateXML(b, T)
xmldata += "</KinBody>"

##########################################################################################
#
# taburet2
#
filename = '../../models/taburet2.off'
bodies, meshes = segmentation.visualize(env, filename, returnmeshes=True)
bbs = [mesh.bounding_box_oriented for mesh in meshes]
bodieslist = [2, 3, 6, 7, 8, 9, 10, 11, 12, 13, 14] # list of bodies which are ok to compute bb
orboxes = [segmentation.bbtoorbox(bbs[i], env) for i in bodieslist]

b = box(0.17, 0.015, 0.03)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   ,  0.   ],
                         [ 0.   ,  1.   ,  0.   , -0.185],
                         [ 0.   ,  0.   ,  1.   ,  0.650],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.17, 0.015, 0.03)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   ,  0.   ],
                         [ 0.   ,  1.   ,  0.   ,  0.185],
                         [ 0.   ,  0.   ,  1.   ,  0.650],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.015, 0.17, 0.015)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   ,  0.185],
                         [ 0.   ,  1.   ,  0.   ,  0.   ],
                         [ 0.   ,  0.   ,  1.   ,  0.635],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.015, 0.17, 0.015)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   , -0.185],
                         [ 0.   ,  1.   ,  0.   ,  0.   ],
                         [ 0.   ,  0.   ,  1.   ,  0.635],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

# little boxes (side 1)
b = box(0.015, 0.014, 0.03)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   ,  0.185],
                         [ 0.   ,  1.   ,  0.   ,  1.39284000e-01],
                         [ 0.   ,  0.   ,  1.   ,  0.650],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.015, 0.014, 0.03)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   ,  0.185],
                         [ 0.   ,  1.   ,  0.   ,  9.27726000e-02],
                         [ 0.   ,  0.   ,  1.   ,  0.650],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.015, 0.014, 0.03)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   ,  0.185],
                         [ 0.   ,  1.   ,  0.   ,  4.62611504e-02],
                         [ 0.   ,  0.   ,  1.   ,  0.650],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.015, 0.014, 0.03)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   ,  0.185],
                         [ 0.   ,  1.   ,  0.   , -2.50300000e-04],
                         [ 0.   ,  0.   ,  1.   ,  0.650],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.015, 0.014, 0.03)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   ,  0.185],
                         [ 0.   ,  1.   ,  0.   , -4.67617500e-02],
                         [ 0.   ,  0.   ,  1.   ,  0.650],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.015, 0.014, 0.03)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   ,  0.185],
                         [ 0.   ,  1.   ,  0.   , -9.32734500e-02],
                         [ 0.   ,  0.   ,  1.   ,  0.650],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.015, 0.014, 0.03)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   ,  0.185],
                         [ 0.   ,  1.   ,  0.   , -1.39784500e-01],
                         [ 0.   ,  0.   ,  1.   ,  0.650],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

# little boxes (side 2)
b = box(0.015, 0.014, 0.03)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   , -0.185],
                         [ 0.   ,  1.   ,  0.   ,  1.39284000e-01],
                         [ 0.   ,  0.   ,  1.   ,  0.650],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.015, 0.014, 0.03)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   , -0.185],
                         [ 0.   ,  1.   ,  0.   ,  9.27726000e-02],
                         [ 0.   ,  0.   ,  1.   ,  0.650],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.015, 0.014, 0.03)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   , -0.185],
                         [ 0.   ,  1.   ,  0.   ,  4.62611504e-02],
                         [ 0.   ,  0.   ,  1.   ,  0.650],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.015, 0.014, 0.03)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   , -0.185],
                         [ 0.   ,  1.   ,  0.   , -2.50300000e-04],
                         [ 0.   ,  0.   ,  1.   ,  0.650],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.015, 0.014, 0.03)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   , -0.185],
                         [ 0.   ,  1.   ,  0.   , -4.67617500e-02],
                         [ 0.   ,  0.   ,  1.   ,  0.650],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.015, 0.014, 0.03)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   , -0.185],
                         [ 0.   ,  1.   ,  0.   , -9.32734500e-02],
                         [ 0.   ,  0.   ,  1.   ,  0.650],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.015, 0.014, 0.03)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   , -0.185],
                         [ 0.   ,  1.   ,  0.   , -1.39784500e-01],
                         [ 0.   ,  0.   ,  1.   ,  0.650],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

# legs
b = box(0.015, 0.015, 0.325)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   ,  0.185],
                         [ 0.   ,  1.   ,  0.   ,  0.185],
                         [ 0.   ,  0.   ,  1.   ,  0.325],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.015, 0.015, 0.325)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   , -0.185],
                         [ 0.   ,  1.   ,  0.   ,  0.185],
                         [ 0.   ,  0.   ,  1.   ,  0.325],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.015, 0.015, 0.325)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   , -0.185],
                         [ 0.   ,  1.   ,  0.   , -0.185],
                         [ 0.   ,  0.   ,  1.   ,  0.325],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

b = box(0.015, 0.015, 0.325)
b.SetTransform(np.array([[ 1.   ,  0.   ,  0.   ,  0.185],
                         [ 0.   ,  1.   ,  0.   , -0.185],
                         [ 0.   ,  0.   ,  1.   ,  0.325],
                         [ 0.   ,  0.   ,  0.   ,  1.   ]]))
orboxes.append((b, b.GetTransform()))

for b in bodies:
    env.Remove(b)

for b in env.GetBodies():
    b.Enable(False)
    b.SetVisible(False)
    
taburet2 = env.ReadKinBodyXMLFile(filename)
env.Add(taburet2)
for l in taburet2.GetLinks():
    for g in l.GetGeometries():
        g.SetTransparency(0.)

xmldata = """<KinBody name="taburet2">
  <body name="main" type="dynamic">
    <Geom type="trimesh" modifiable="false">
      <data>taburet2.off 1.0</data>
      <render>taburet2.off 1.0</render>
    </Geom>
  </body>\n"""

for b, T in orboxes:
    xmldata += CreateXML(b, T)
xmldata += "</KinBody>"

##########################################################################################
#
# square
#
filename = '../../models/square_scaled.off'
bodies, meshes = segmentation.visualize(env, filename, returnmeshes=True)
bbs = [mesh.bounding_box_oriented for mesh in meshes]
bodieslist = [5, 7, 8, 9] # list of bodies which are ok to compute bb
orboxes = [segmentation.bbtoorbox(bbs[i], env) for i in bodieslist]
scale = 0.55
b = box(0.475, 0.1, 0.0152905, scale=scale)
T = np.array([[ 1.   ,  0.   ,  0.   ,  0.        ],
              [ 0.   ,  1.   ,  0.   , -0.4985    ],
              [ 0.   ,  0.   ,  1.   ,  0.99266952],
              [ 0.   ,  0.   ,  0.   ,  1.        ]])
T[0:3, 3] *= scale
b.SetTransform(T)
orboxes.append((b, b.GetTransform()))

b = box(0.475, 0.1, 0.0152905, scale=scale)
T = np.array([[ 1.   ,  0.   ,  0.   ,  0.        ],
              [ 0.   ,  1.   ,  0.   ,  0.4985    ],
              [ 0.   ,  0.   ,  1.   ,  0.99266952],
              [ 0.   ,  0.   ,  0.   ,  1.        ]])
T[0:3, 3] *= scale
b.SetTransform(T)
orboxes.append((b, b.GetTransform()))

b = box(0.1, 0.475, 0.0152905, scale=scale)
T = np.array([[ 1.   ,  0.   ,  0.   ,  0.4985    ],
              [ 0.   ,  1.   ,  0.   ,  0.        ],
              [ 0.   ,  0.   ,  1.   ,  0.99266952],
              [ 0.   ,  0.   ,  0.   ,  1.        ]])
T[0:3, 3] *= scale
b.SetTransform(T)
orboxes.append((b, b.GetTransform()))

b = box(0.1, 0.475, 0.0152905, scale=scale)
T = np.array([[ 1.   ,  0.   ,  0.   , -0.4985    ],
              [ 0.   ,  1.   ,  0.   ,  0.        ],
              [ 0.   ,  0.   ,  1.   ,  0.99266952],
              [ 0.   ,  0.   ,  0.   ,  1.        ]])

T[0:3, 3] *= scale
b.SetTransform(T)
orboxes.append((b, b.GetTransform()))

# x-pattern
b = box(0.375, 0.037905, 0.0224684, scale=scale)
T = np.array([[  7.07106795e-01,   0.00000000e+00,   7.07106767e-01,   0.],
              [  7.07106767e-01,  -2.22044605e-16,  -7.07106795e-01,   0.],
              [  0.00000000e+00,   1.00000000e+00,  -2.22044605e-16,   9.28332090e-01],
              [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]])
T[0:3, 3] *= scale
b.SetTransform(T)
orboxes.append((b, b.GetTransform()))

b = box(0.375, 0.037905, 0.0224684, scale=scale)
T = np.array([[ -7.07106767e-01,   5.55111512e-17,   7.07106795e-01,   3.12598965e-18],
              [  7.07106795e-01,  -3.33066907e-16,   7.07106767e-01,  -7.34147004e-17],
              [  3.88578059e-16,   1.00000000e+00,   2.22044605e-16,   9.28332090e-01],
              [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]])
T[0:3, 3] *= scale
b.SetTransform(T)
orboxes.append((b, b.GetTransform()))

# support
b = box(0.0224684, 0.135, 0.022, scale=scale)
T = np.array([[ -7.07106811e-01,  -3.79928176e-01,  -5.96367789e-01,   3.840e-01    ],
              [  7.07106752e-01,  -3.79928212e-01,  -5.96367836e-01,   3.840e-01    ],
              [ -2.92076752e-09,  -8.43391448e-01,   5.37299605e-01,   8.41914177e-01],
              [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]])
T[0:3, 3] *= scale
b.SetTransform(T)
orboxes.append((b, b.GetTransform()))

b = box(0.0224684, 0.135, 0.022, scale=scale)
T = np.array([[ -7.07106752e-01,   3.79928212e-01,   5.96367836e-01,  -3.84000000e-01],
              [ -7.07106811e-01,  -3.79928176e-01,  -5.96367788e-01,   3.84000000e-01],
              [ -3.38015144e-09,  -8.43391448e-01,   5.37299605e-01,   8.41914177e-01],
              [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]])
T[0:3, 3] *= scale
b.SetTransform(T)
orboxes.append((b, b.GetTransform()))

b = box(0.0224684, 0.135, 0.022, scale=scale)
T = np.array([[  7.07106811e-01,   3.79928176e-01,   5.96367788e-01,  -3.82500000e-01],
              [ -7.07106752e-01,   3.79928212e-01,   5.96367836e-01,  -3.82500000e-01],
              [ -3.38015144e-09,  -8.43391448e-01,   5.37299605e-01,   8.41914177e-01],
              [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]])
T[0:3, 3] *= scale
b.SetTransform(T)
orboxes.append((b, b.GetTransform()))

b = box(0.0224684, 0.135, 0.022, scale=scale)
T = np.array([[  7.07106731e-01,  -3.79928220e-01,  -5.96367855e-01,   3.81850000e-01],
              [  7.07106831e-01,   3.79928164e-01,   5.96367772e-01,  -3.81850000e-01],
              [ -1.37105272e-09,  -8.43391450e-01,   5.37299602e-01,   8.41914058e-01],
              [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]])
T[0:3, 3] *= scale
b.SetTransform(T)
orboxes.append((b, b.GetTransform()))

# legs
b = box(0.0224684, 0.03, 0.47, scale=scale)
T = np.array([[  7.07106783e-01,  -6.96997605e-01,  -1.19139983e-01,   3.48130709e-01],
              [  7.07106779e-01,   6.96997609e-01,   1.19139984e-01,  -3.48130709e-01],
              [  5.27324504e-11,  -1.68489380e-01,   9.85703469e-01,   4.71873615e-01],
              [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]])
T[0:3, 3] *= scale
b.SetTransform(T)
orboxes.append((b, b.GetTransform()))

b = box(0.0224684, 0.03, 0.47, scale=scale)
T = np.array([[  7.07106779e-01,   6.96997609e-01,   1.19139984e-01,  -3.48130709e-01],
              [ -7.07106783e-01,   6.96997605e-01,   1.19139983e-01,  -3.48130709e-01],
              [  5.27324434e-11,  -1.68489380e-01,   9.85703469e-01,   4.71873615e-01],
              [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]])
T[0:3, 3] *= scale
b.SetTransform(T)
orboxes.append((b, b.GetTransform()))

b = box(0.0224684, 0.03, 0.47, scale=scale)
T = np.array([[ -7.07106783e-01,   6.96997605e-01,   1.19139983e-01,  -3.48130709e-01],
              [ -7.07106779e-01,  -6.96997609e-01,  -1.19139984e-01,   3.48130709e-01],
              [  5.27324365e-11,  -1.68489380e-01,   9.85703469e-01,   4.71873615e-01],
              [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]])
T[0:3, 3] *= scale
b.SetTransform(T)
orboxes.append((b, b.GetTransform()))

b = box(0.0224684, 0.03, 0.47, scale=scale)
T = np.array([[ -7.07106779e-01,  -6.96997609e-01,  -1.19139984e-01,   3.48130709e-01],
              [  7.07106783e-01,  -6.96997605e-01,  -1.19139983e-01,   3.48130709e-01],
              [  5.27324712e-11,  -1.68489380e-01,   9.85703469e-01,   4.71873615e-01],
              [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   1.00000000e+00]])
T[0:3, 3] *= scale
b.SetTransform(T)
orboxes.append((b, b.GetTransform()))

for b in bodies:
    env.Remove(b)

for b in env.GetBodies():
    b.Enable(False)
    b.SetVisible(False)
    
square = env.ReadKinBodyXMLFile(filename)
env.Add(square)
for l in square.GetLinks():
    for g in l.GetGeometries():
        g.SetTransparency(0.)

xmldata = """<KinBody name="square">
  <body name="main" type="dynamic">
    <Geom type="trimesh" modifiable="false">
      <data>square.off 1.0</data>
      <render>square.off 1.0</render>
    </Geom>
  </body>\n"""

for b, T in orboxes:
    xmldata += CreateXML(b, T)
xmldata += "</KinBody>"

##########################################################################################
#
# praktrik
#
filename = '../../models/praktrik.off'
bodies, meshes = segmentation.visualize(env, filename, returnmeshes=True)
bbs = [mesh.bounding_box_oriented for mesh in meshes]

bodieslist = [0, 1, 2, 3, 4, 5, 6] # list of bodies which are ok to compute bb
orboxes = [segmentation.bbtoorbox(bbs[i], env) for i in bodieslist]

for b in bodies:
    env.Remove(b)

for b in env.GetBodies():
    b.Enable(False)
    b.SetVisible(False)
    
xmldata = """<KinBody name="praktrik-square">
  <body name="main" type="dynamic">
    <Geom type="trimesh" modifiable="false">
      <data>praktrik-square.off 1.0</data>
      <render>praktrik-square.off 1.0</render>
    </Geom>
  </body>\n"""

for b, T in orboxes:
    xmldata += CreateXML(b, T)
xmldata += "</KinBody>"

# All-boxes model
xmldata = """<KinBody name="praktrik3">
"""

for b, T in orboxes:
    xmldata += CreateXML(b, T, True, True)
xmldata += "</KinBody>"


##########################################################################################
#
# hamilton spot table
#
filename = '../../models/hamilton-spot.off'
bodies, meshes = segmentation.visualize(env, filename, returnmeshes=True)
bbs = [mesh.bounding_box_oriented for mesh in meshes]


##########################################################################################
#
# isom table
#
filename = '../../models/isom-table.off'
bodies, meshes = segmentation.visualize(env, filename, returnmeshes=True)
bbs = [mesh.bounding_box_oriented for mesh in meshes]

##########################################################################################
#
# forte table
#
filename = '../../models/forte.off'
bodies, meshes = segmentation.visualize(env, filename, returnmeshes=True)
bbs = [mesh.bounding_box_oriented for mesh in meshes]

bodieslist = [8, 9, 10, 11, 12, 13, 14, 15, 16] # list of bodies which are ok to compute bb
orboxes = [segmentation.bbtoorbox(bbs[i], env) for i in bodieslist]

for b in bodies:
    env.Remove(b)

for b in env.GetBodies():
    b.Enable(False)
    b.SetVisible(False)
    
xmldata = """<KinBody name="forte">
  <body name="main" type="dynamic">
    <Geom type="trimesh" modifiable="false">
      <data>forte.off 1.0</data>
      <render>forte.off 1.0</render>
    </Geom>
  </body>\n"""

for b, T in orboxes:
    xmldata += CreateXML(b, T)
xmldata += "</KinBody>"

