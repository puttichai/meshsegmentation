import openravepy as orpy
import numpy as np
import trimesh as tm
from itertools import chain # for unpacking nested iterables
from os.path import splitext
import random
rng = random.SystemRandom()
epsilon = 1e-8

def segment(mesh, grouping):
    """segment takes a mesh and segmentation data (computed by CGAL) as inputs. It creates a
    set of new meshes. Each new mesh consists of faces from the original mesh grouped
    together by the mesh segmentation algorithm.

    Parameters
    ----------
    mesh : trimesh.Trimesh
        A triangle mesh of the object of interest
    grouping : list of integers
        List of integers where each integer indicates the segment index of the 
        corresponding face

    Returns
    -------
    segmented_meshes : list
       List of meshes

    """
    meshfaces = mesh._data['faces'] # prevent some processing when calling mesh.faces
    assert(len(meshfaces) == len(grouping))

    segmented_meshes = []
    
    groups = []
    distinct_groups = set(grouping)
    for g in distinct_groups:
        groups.append([]) # create a empty list for each group
    # Put face indices into their corresponding groups
    for (iface, igroup) in enumerate(grouping):
        groups[igroup].append(iface)
    # Now each group in groups is a list of face indices
        
    for group in groups:
        faces = [meshfaces[iface] for iface in group]
        unique_vertex_indices = sorted(set(chain.from_iterable(faces)))

        # conv contains the mapping between the new and the old vertex indices
        conv = dict()
        conv.update(enumerate(unique_vertex_indices))
        
        inv = {v: k for (k, v) in conv.iteritems()} # inverse mapping

        mesh_vertices = []
        for (newindex, oldindex) in conv.iteritems():
            mesh_vertices.append(mesh.vertices[oldindex])
        mesh_faces = []
        for face in faces:
            mesh_faces.append([inv[v] for v in face])

        segmented_mesh = tm.Trimesh(mesh_vertices, mesh_faces)
        segmented_meshes.append(segmented_mesh)
    return segmented_meshes
    
    
def visualize(orenv, meshfilename, kinbodybasename='b', returnmeshes=False):
    """Visualize mesh segmentation results in an OpenRAVE environment. Meshes of different
    parts are shown in different random colors.

    Parameters
    ----------
    orenv : openravepy.Environment
    meshfilename : string
        A file name of the original mesh (in off format)
    kinbodybasename : string
        Base name for KinBodys created for visualization
    returnmeshes : bool
        Indicates if meshes (trimesh.Trimesh) is to be returned

    Returns
    -------
    bodies : list
        List of openravepy.KinBody
    segmented_meshes : list
        List of meshes

    """
    mesh = tm.load_mesh(meshfilename, 'off', process=False)
    basename = splitext(meshfilename)[0]
    datafilename = "{0}.segmentation.data".format(basename)
    with open(datafilename, 'r') as f:
        data = f.read()
    grouping = [int(s) for s in data.split(' ')]

    # Process segmentation data
    segmented_meshes = segment(mesh, grouping)

    # Create OpenRAVE Trimesh accordingly
    ormeshes = [orpy.TriMesh(m.vertices, m.faces) for m in segmented_meshes]
    bodies = []
    for (i, m) in enumerate(ormeshes):
        b = orpy.RaveCreateKinBody(orenv, '')
        b.InitFromTrimesh(m)
        b.SetName('{0}{1:02d}'.format(kinbodybasename, i))
        color = [rng.random() for _ in xrange(3)]
        b.GetLinks()[0].GetGeometries()[0].SetDiffuseColor(color)
        orenv.Add(b)
        bodies.append(b)
    if returnmeshes:
        return bodies, segmented_meshes
    else:
        return bodies

  
def norm(vector):
    return np.sqrt(np.dot(vector, vector))


def normalize(vector):
    length = np.sqrt(np.dot(vector, vector))
    if length > 0:
        return vector/length
    raise ValueError, "vector has zero length"


def areparallel(vect1, vect2):
    v1 = normalize(vect1)
    v2 = normalize(vect2)
    if abs(abs(np.dot(v1, v2)) - 1) <= epsilon:
        return True
    else:
        return False

    
def bbtoorbox(bb, orenv, kinbodyname='b', color=[0.5, 1, 0.5]):
    """Convert a trimesh's box primitive to an OpenRAVE box.

    (px, py, pz): the object's origin in the world frame
    (dx, dy, dz): half-extents of the box in x, y, z, respectively    
    """
    p = np.mean(bb.vertices, axis=0) # the bb's center
    axes = [] # contains three axes of the local frame
    dim = [] # half-extents of the box along axes speficied in axes
    
    # Although trimesh already provides face_normals, it is easier for us to calculate the
    # normals this way since we also need the distance from each facet to the center
    for facet in bb.facets():
        faces = [bb.faces[iface] for iface in facet] # faces included in this facet
        vertex_indices = sorted(set(chain.from_iterable(faces)))
        vcenter = np.mean([bb.vertices[i] for i in vertex_indices], axis=0)
        direction = (vcenter - p)
        axis = normalize(direction)
        parallel = False
        for existingaxis in axes:
            if areparallel(axis, existingaxis):
                parallel = True
                break
        if not parallel:
            axes.append(axis)
            dim.append(norm(direction))
            if len(axes) == 3:
                break
    assert (len(axes) == 3), "Unable to compute box's local frame"
    # Make sure the axes respect the right-hand rule
    if np.dot(np.cross(axes[0], axes[1]), axes[2]) < 0:
        axes.append(axes.pop(1)) # swap the last two elements
        dim.append(dim.pop(1)) # swap the last two elements

    Tb = np.vstack([np.vstack([axes[0], axes[1], axes[2], p]).T, np.array([0, 0, 0, 1])])
        
    orbox = orpy.RaveCreateKinBody(orenv, '')
    orbox.InitFromBoxes(np.array([[0.0, 0.0, 0.0, dim[0], dim[1], dim[2]]]))
    orbox.SetName(kinbodyname)
    orenv.Add(orbox, True) # allow name clash (fixing it by appending numbers to the name)
    orbox.SetTransform(Tb)
    return orbox, Tb
