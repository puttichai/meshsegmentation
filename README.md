Mesh Segmentation
=================

Code snippets for mesh segmentation using a package from CGAL.

Dependencies
============
* [CGAL](https://github.com/CGAL/cgal)

* [OpenRAVE](https://github.com/rdiankov/openrave/tree/production) for visualization

* python `trimesh` package
```
sudo pip install trimesh[all]
```

Usage
=====
* Mesh segmentation
From `src/cpp` directory, do

```
./segment <filename-off> <options>
```

to compute segmentation of a mesh from an off file. For avaiable options, see `segment.cpp` for more details.

* Visualization (using OpenRAVE)

```
python visualize.py -f <filename>
```
